// Importing the necessary libraries
#include <ESP8266WiFi.h> // WiFi
#include <PubSubClient.h> // MQTT, the protocol used to communicate with the server
#include <WiFiClientSecure.h> // Wifi client, used by MQTT
#include <ArduinoJson.h> // JSON, used for the formatting of messages sent to the server
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <EEPROM.h> // EEPROM for saving the WiFi SSID and password
#include <DNSServer.h> // DNS server to redirect wifi clients to the web server
#include <ESP8266mDNS.h> // Another DNS server to help find the device on a network

#include "axis_controller.cpp" // A Custom made class to help with motor control

// Information to identify the device
#define DEVICE_TYPE "pantilt"
#define DEVICE_FIRMWARE_VERSION "0.0.4"

// EEPROM
#define EEPROM_END_BYTE 0x0A
#define EEPROM_MAX_ITEM_LENGTH 50
#define EEPROM_WIFI_SSID_ADDRESS 0
#define EEPROM_WIFI_PASSWORD_ADDRESS 100
#define EEPROM_MQTT_USERNAME_ADDRESS 200
#define EEPROM_MQTT_PASSWORD_ADDRESS 300
#define EEPROM_DEVICE_NICKNAME_ADDRESS 400

// MQTT settings
#define MQTT_RECONNECT_PERIOD 1000
#define MQTT_BROKER_ADDRESS "mqtt.iot.maximemoreillon.com"
#define MQTT_PORT 30883 // MQTT with SSL
#define MQTT_QOS 1
#define MQTT_RETAIN true

// Wifi Settings
#define WIFI_STA_CONNECTION_TIMEOUT 20000
#define WIFI_AP_IP IPAddress(192, 168, 4, 1)

// MISC
#define DNS_PORT 53

// Instanciate objects

// Controller for the horizontal axis using pins D6 and D5
AxisController horizontal(D6, D5); 

// Controller for the vertical axis using pins D1 and D2
AxisController vertical(D1, D2); 

// Wifi client to communicate via WiFi (SSL encrypted)
WiFiClientSecure wifi_client; 

// MQTT client that uses the WiFi client for communication
PubSubClient MQTT_client(wifi_client); 

// A web server to set the WiFI SSID and password via a web page
AsyncWebServer web_server(80);

// DNS server to redirect clients of the WiFi access point to the web server
// Used only when device in access point mode
DNSServer dns_server;


// Global variables
String wifi_mode = "STA";


void setup() {
  // This is the code that runs when the device starts

  //Setup the serial (USB) interface for debugging
  serial_setup();

  // Setup the internal memory so as to access the saved wifi settings
  EEPROM.begin(512);

  // Initialize the interface with the motors
  horizontal.init();
  vertical.init();

  // Ignore certificates (traffic will still be encrypted)
  wifi_client.setInsecure();
  
  // Configure the wifi
  wifi_setup();
  
  // Setup MQTT client
  MQTT_setup();

  //Start the web server to serve webpages allowing users to configure wifi
  web_server_setup();

  // DNS server for when the device is in access point mode
  dns_server.start(DNS_PORT, "*", WIFI_AP_IP);

  // Help clients find the device on the network at pantilt<Serial no>.local
  MDNS.begin(get_device_name().c_str());
}

void loop() {
  // This code runs over and over in a loop after setup() is executed

  // Handles the MQTT connection to the server
  MQTT_connection_manager(); 

  // Handles MQTT messages coming from the server
  MQTT_client.loop(); 

  // Manage the DNS to help network clients to find the device
  MDNS.update(); 

  // Manage motors
  horizontal.handle();
  vertical.handle();

  // Handle DNS requests, i.e. redirect clients to web server
  dns_server.processNextRequest();
}
